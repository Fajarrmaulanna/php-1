<?php 

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

//animal.php
$sheep = new animal("shaun");
echo "Name : " . $sheep->get_name() . "<br>"; // "shaun"
echo "legs : " . $sheep->legs  . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded  . "<br><br>"; // "no"

//Frog.php
$kodok = new Frog ("buduk");
echo "Name : " . $kodok->get_name() . "<br>"; 
echo "Legs : " . $kodok->legs  . "<br>"; 
echo "Cold blooded : " . $kodok->cold_blooded  . "<br>";
$kodok->jump();

//Ape.php
$sungokong = new Ape ("kera sakti");
echo "Name : " . $sungokong->get_name() . "<br>"; 
echo "Legs : " . $sungokong->legs  . "<br>"; 
echo "Cold blooded : " . $sungokong->cold_blooded  . "<br>";
$sungokong->yell();


?>